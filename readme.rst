PFLOTRAN-OGS-1.1
--------

PFLOTRAN-OGS-1.1 is a fork of PFLOTRAN (7-Sept-2018)
This release is supported by OpenGoSim (OGS), which focuses on the development of Oil & Gas modules in PFLOTRAN.
For any query please email support at opengosim dot com

PFLOTRAN is an open source, state-of-the-art massively parallel subsurface flow and reactive transport code. The code is developed under a GNU LGPL license allowing for third parties to interface proprietary software with the code, however any modifications to the code itself must be documented and remain open source.  PFLOTRAN is written in object oriented, free formatted Fortran 2003.  The choice of Fortran over C/C++ was based primarily on the need to enlist and preserve tight collaboration with *experienced* domain scientists, without which PFLOTRAN's sophisticated process models would not exist.
For more information see https://www.pflotran.org